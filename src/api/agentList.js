import request from '@/utils/request'

export function agentList(params) {
  return request({
    url: '/agent/list',
    method: 'get',
    params: params
  })
}

export function createAgent(data) {
  return request({
    url: '/agent',
    method: 'post',
    data: data
  })
}

export function updateAgent(id, data) {
  return request({
    url: '/agent/' + id,
    method: 'put',
    data: data
  })
}

export function deleteAgent(id) {
  return request({
    url: '/agent/' + id,
    method: 'delete'
  })
}
