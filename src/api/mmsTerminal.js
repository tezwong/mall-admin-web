import request from '@/utils/request'

export function deviceList(params) {
  return request({
    url: '/mms/mmsTerminal/list',
    method: 'get',
    params: params
  })
}

export function createDevice(data) {
  return request({
    url: '/mms/mmsTerminal',
    method: 'post',
    data: data
  })
}

export function updateDevice(id, data) {
  return request({
    url: '/mms/mmsTerminal/' + id,
    method: 'put',
    data: data
  })
}

export function deleteDevice(id) {
  return request({
    url: '/mms/mmsTerminal/' + id,
    method: 'delete'
  })
}
