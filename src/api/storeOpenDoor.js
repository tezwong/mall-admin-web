import request from '@/utils/request'

export function openRecordList(params) {
  return request({
    url: '/mms/storeOpenDoor/list',
    method: 'get',
    params: params
  })
}

export function createOpenRecord(data) {
  return request({
    url: '/mms/storeOpenDoor',
    method: 'post',
    data: data
  })
}

export function updateOpenRecord(id, data) {
  return request({
    url: '/mms/storeOpenDoor/' + id,
    method: 'put',
    data: data
  })
}

export function deleteOpenRecord(id) {
  return request({
    url: '/mms/storeOpenDoor/' + id,
    method: 'delete'
  })
}
