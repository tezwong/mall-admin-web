import request from '@/utils/request'

export function storeList(params) {
  return request({
    url: '/mms/store/list',
    method: 'get',
    params: params
  })
}

export function createStore(data) {
  return request({
    url: '/mms/store',
    method: 'post',
    data: data
  })
}

export function updateStore(id, data) {
  return request({
    url: '/mms/store/' + id,
    method: 'put',
    data: data
  })
}

export function deleteStore(id) {
  return request({
    url: '/mms/store/' + id,
    method: 'delete'
  })
}
